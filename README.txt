Version: 0.01
Made by efflicto
Contact: mailto:dsbackup@0x05.de

Wiki: https://gitlab.com/efflicto/DarkSoulsBackup/wikis/home

This software:
I'm making no warranties, express or implied regarding the fitness of this software. I claim no liabilitly for data loss or other problems caused directly or indirectly by this software. The user is assuming the entire risk as to the software's quality and accuracy.
Dark Souls ™ are registered trademark of BANDAI NAMCO Entertainment Inc

Links:
I'm not responsible for any content on pages which I'm linking to. If any damages occur by using information on referred pages, only the author of the respective page may be held liable

Bugs:
This software is still in development. Please report any bugs here: https://gitlab.com/efflicto/DarkSoulsBackup/issues

Open source:
Sourcecode: https://gitlab.com/efflicto/DarkSoulsBackup
Licensed under the MIT license: https://gitlab.com/efflicto/DarkSoulsBackup/blob/master/LICENSE.txt

Third-party software and graphics:
Program Icon: http://www.famfamfam.com/
ZIP Library: http://dotnetzip.codeplex.com/
Fast Directory Enumerator: http://www.codeproject.com/Articles/38959/A-Faster-Directory-Enumerator
Folder Select Dialog: http://www.lyquidity.com/devblog/?p=136