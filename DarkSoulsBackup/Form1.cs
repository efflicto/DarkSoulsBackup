﻿using DarkSoulsBackup.utils;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using Ionic.Zip;
using System.IO;

namespace DarkSoulsBackup {
    public partial class Form1 :Form {

        private utils.Backup ba;
        private PerformanceCounter theCPUCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        #region FormInit
        public Form1() {
            //Setup the gui
            InitializeComponent();
            this.setTooltips();
            richTextBox1.LinkClicked += richTextBox1_LinkClicked;
            textBox1.Text = DarkSoulsBackup.Properties.Settings.Default.savePath;
            if(DarkSoulsBackup.Properties.Settings.Default.backupPath.Equals("")) {
                textBox2.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString();
                DarkSoulsBackup.Properties.Settings.Default.backupPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                DarkSoulsBackup.Properties.Settings.Default.Save();
            } else {
                textBox2.Text = DarkSoulsBackup.Properties.Settings.Default.backupPath;
            }
            if(DarkSoulsBackup.Properties.Settings.Default.savePath.Equals("")) {
                if(Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\DarkSoulsIII")) {
                    textBox2.Text = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\DarkSoulsIII";
                    DarkSoulsBackup.Properties.Settings.Default.savePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\DarkSoulsIII";
                    DarkSoulsBackup.Properties.Settings.Default.Save();
                }
            } else {
                textBox2.Text = DarkSoulsBackup.Properties.Settings.Default.backupPath;
            }
            checkBox2.Checked = DarkSoulsBackup.Properties.Settings.Default.backupOnStartup;
            checkBox3.Checked = DarkSoulsBackup.Properties.Settings.Default.backupKeepDaily;
            checkBox4.Checked = DarkSoulsBackup.Properties.Settings.Default.startupMinimized;
            checkBox5.Checked = DarkSoulsBackup.Properties.Settings.Default.backupKeepWeekly;
            numericUpDown1.Value = DarkSoulsBackup.Properties.Settings.Default.backupSchedule;
            numericUpDown2.Value = DarkSoulsBackup.Properties.Settings.Default.backupCount;
            toolStripStatusLabel2.Spring = true;
            switch(DarkSoulsBackup.Properties.Settings.Default.commpressionLevel.ToString()) {
                case "BestCompression":
                    comboBox1.SelectedIndex = 2;
                    break;
                case "Default":
                    comboBox1.SelectedIndex = 0;
                    break;
                case "BestSpeed":
                    comboBox1.SelectedIndex = 1;
                    break;
                default:
                    break;
            }
            if(IsStartupItem()) {
                checkBox1.Checked = true;
            } else {
                checkBox1.Checked = false;
            }
            //Setup the backup timer
            timer1.Interval = DarkSoulsBackup.Properties.Settings.Default.backupSchedule * 1000 * 60;
            //if the user selected to make a backup on startup it performs a backup on start and starts the interval timer
            if(DarkSoulsBackup.Properties.Settings.Default.backupOnStartup) {
                timer1.Enabled = true;
                button8.Enabled = false;
                button9.Enabled = true;
                toolStripStatusLabel1.Text = "Interval timer started.";

                if(!backgroundWorkerBackup.IsBusy) {
                    backgroundWorkerBackup.RunWorkerAsync();
                }
            } else {
                timer1.Enabled = false;
                button8.Enabled = true;
                button9.Enabled = false;
                toolStripStatusLabel1.Text = "Interval timer stopped.";
            }
        }
        #endregion


        #region Timers
        /// <summary>
        /// Performs a backup on every timer tick (interval configured by user)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e) {
            try {
                if(!backgroundWorkerBackup.IsBusy) {
                    backgroundWorkerBackup.RunWorkerAsync();
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Updates the statustoolstrip with CPU and RAM usage.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e) {
            try {
                Process currentProcess = System.Diagnostics.Process.GetCurrentProcess();
                long totalBytesOfMemoryUsed = currentProcess.WorkingSet64 / 1024 / 1024;
                this.theCPUCounter.NextValue();
                toolStripStatusLabel2.Text = string.Format("Current CPU usage: {0}% | RAM usage: {1}MB",
                    this.theCPUCounter.NextValue().ToString(),
                    totalBytesOfMemoryUsed.ToString()
                    );
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        #endregion


        #region Functions
        /// <summary>
        /// Checks if the paths are set correctly
        /// </summary>
        /// <returns></returns>
        private bool IsPathsSet() {
            try {
                if(!DarkSoulsBackup.Properties.Settings.Default.backupPath.Equals("") && !DarkSoulsBackup.Properties.Settings.Default.savePath.Equals("")) {
                    return true;
                } else {
                    return false;
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
        }
        /// <summary>
        /// Checks if the program is in autostart
        /// </summary>
        /// <returns></returns>
        private bool IsStartupItem() {
            try {
                // The path to the key where Windows looks for startup applications
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                if(rkApp.GetValue("DarkSoulsBackup") == null)
                    // The value doesn't exist, the application is not set to run at startup
                    return false;
                else
                    // The value exists, the application is set to run at startup
                    return true;
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
        }
        /// <summary>
        /// Saves all program settings
        /// </summary>
        private void SaveSettings() {
            try {
                DarkSoulsBackup.Properties.Settings.Default.savePath = textBox1.Text;
                if(!Directory.Exists(DarkSoulsBackup.Properties.Settings.Default.savePath)) {
                    Directory.CreateDirectory(DarkSoulsBackup.Properties.Settings.Default.savePath);
                }
                DarkSoulsBackup.Properties.Settings.Default.backupPath = textBox2.Text;
                DarkSoulsBackup.Properties.Settings.Default.backupOnStartup = checkBox2.Checked;
                DarkSoulsBackup.Properties.Settings.Default.backupKeepDaily = checkBox3.Checked;
                DarkSoulsBackup.Properties.Settings.Default.backupKeepWeekly = checkBox5.Checked;
                DarkSoulsBackup.Properties.Settings.Default.backupCount = (int)numericUpDown2.Value;
                DarkSoulsBackup.Properties.Settings.Default.backupSchedule = (int)numericUpDown1.Value;
                DarkSoulsBackup.Properties.Settings.Default.startupMinimized = checkBox4.Checked;

                switch(comboBox1.SelectedIndex) {
                    case 2:
                        DarkSoulsBackup.Properties.Settings.Default.commpressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                        break;
                    case 0:
                        DarkSoulsBackup.Properties.Settings.Default.commpressionLevel = Ionic.Zlib.CompressionLevel.Default;
                        break;
                    case 1:
                        DarkSoulsBackup.Properties.Settings.Default.commpressionLevel = Ionic.Zlib.CompressionLevel.BestSpeed;
                        break;
                    default:
                        break;
                }
                timer1.Interval = DarkSoulsBackup.Properties.Settings.Default.backupSchedule * 60 * 1000;
                DarkSoulsBackup.Properties.Settings.Default.Save();
                toolStripStatusLabel1.Text = "Settings saved.";
                Debug.WriteLine(DarkSoulsBackup.Properties.Settings.Default.backupSchedule);
                Debug.WriteLine(timer1.Interval);
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            
        }
        #endregion


        #region GUI
        /// <summary>
        /// Enables/Disables autostart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox1_CheckedChanged(object sender, EventArgs e) {
            try {
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if(checkBox1.Checked) {
                    if(!IsStartupItem())
                        // Add the value in the registry so that the application runs at startup
                        rkApp.SetValue("DarkSoulsBackup", Application.ExecutablePath.ToString());
                }
                if(!checkBox1.Checked) {
                    rkApp.DeleteValue("DarkSoulsBackup", false);
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// On minimizing disable the taskbar icon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Resize(object sender, EventArgs e) {
            try {
                if(this.WindowState == FormWindowState.Minimized) {
                    if(this.ShowInTaskbar) {
                        this.ShowInTaskbar = false;
                    }
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Shows the program if user clicks on the try icon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notifyIcon1_DoubleClick(object sender, EventArgs e) {
            try {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                this.Show();
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// On close ask the user if he wants to
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            //Currently bugged - rework needed.
            /*DialogResult dialog = MessageBox.Show("Do you really want to close the program?\nMake sure you saved your settings!", "Dark Souls Backup", MessageBoxButtons.YesNo);
            if(dialog == DialogResult.No) {
                e.Cancel = true;
            }*/
        }
        /// <summary>
        /// Program close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                Application.Exit();
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// If the user enables it, start minimized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e) {
            try {
                if(DarkSoulsBackup.Properties.Settings.Default.startupMinimized) {
                    this.WindowState = FormWindowState.Minimized;
                    toolStripStatusLabel1.Text = DarkSoulsBackup.Properties.Settings.Default.startupMinimized.ToString();
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// process links in the richtextbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void richTextBox1_LinkClicked(object sender, LinkClickedEventArgs e) {
            try {
                System.Diagnostics.Process.Start(e.LinkText);
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Show the about tab if user clicks on the about button on the tray icon context menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                this.Show();
                tabControl1.SelectedTab = tabPage2;
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }
        #endregion


        #region Buttons
        /// <summary>
        /// Select the source path
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e) {
            try {
                FolderSelect.FolderSelectDialog dia = new FolderSelect.FolderSelectDialog();
                dia.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\DarkSoulsIII";
                if(dia.ShowDialog()) {
                    textBox1.Text = dia.FileName;
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Resets the source path to the default
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e) {
            try {
                textBox1.Text = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\DarkSoulsIII";
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Select the destination path
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e) {
            try {
                FolderSelect.FolderSelectDialog dia = new FolderSelect.FolderSelectDialog();
                dia.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                if(dia.ShowDialog()) {
                    textBox2.Text = dia.FileName;
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Resets the destination path to default
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e) {
            try {
                textBox2.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString()+"\\DarkSoulsBackup";
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Saves settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e) {
            try {
                SaveSettings();
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error saving settings\nError: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Saves settings and minimizes the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e) {
            try {
                SaveSettings();
                this.WindowState = FormWindowState.Minimized;
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error saving settings\nError: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Starts the backup interval
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button8_Click(object sender, EventArgs e) {
            try {
                timer1.Enabled = true;
                button8.Enabled = false;
                button9.Enabled = true;
                toolStripStatusLabel1.Text = "Interval timer started.";
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Stopps the interval timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button9_Click(object sender, EventArgs e) {
            try {
                if(timer1.Enabled) {
                    timer1.Enabled = false;
                    button9.Enabled = false;
                    button8.Enabled = true;
                    toolStripStatusLabel1.Text = "Interval timer stopped.";
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Program close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e) {
            try {
                Application.Exit();
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Perform a manual backup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button10_Click(object sender, EventArgs e) {
            try {
                if(!backgroundWorkerManualBackup.IsBusy) {
                    backgroundWorkerManualBackup.RunWorkerAsync();
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Opens the savegame folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button11_Click(object sender, EventArgs e) {
            try {
                Process.Start(textBox1.Text);
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error opening folder. Please select an existing folder.\nError: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Opens the backup folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button12_Click(object sender, EventArgs e) {
            try {
                Process.Start(textBox2.Text);
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error opening folder. Please select an existing folder.\nError: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        #endregion


        #region Backgroundworkers
        /// <summary>
        /// Backgroundworker start method to abstract the GUI from the backup logic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorkerBackup_DoWork(object sender, DoWorkEventArgs e) {
            try {
                if(IsPathsSet()) {
                    ba = new utils.Backup();
                    ba.doAutomaticBackup();
                    Debug.WriteLine(timer1.Interval);
                } else {
                    MessageBox.Show("Paths are not set correctly or saved", "Dark Souls Backup: Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Backgroundworker progress changed event listener
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorkerBackup_ProgressChanged(object sender, ProgressChangedEventArgs e) {

        }
        /// <summary>
        /// Backgroundworker completed event listener
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorkerBackup_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            try {
                toolStripStatusLabel3.Text = "Last backup done on: " + DateTime.Now.ToString();
                ba = null;
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Manual backgroundworker start method to abstract the GUI from the backup logic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorkerManualBackup_DoWork(object sender, DoWorkEventArgs e) {
            try {
                if(IsPathsSet()) {
                    ba = new utils.Backup();
                    ba.doManualBackup();
                } else {
                    MessageBox.Show("Paths are not set correctly or saved", "Dark Souls Backup: Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Backgroundworker progress changed event listener
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorkerManualBackup_ProgressChanged(object sender, ProgressChangedEventArgs e) {
        }
        /// <summary>
        /// Backgroundworker completed event listener
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorkerManualBackup_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            try {
                toolStripStatusLabel3.Text = "Manual backup done on: " + DateTime.Now.ToString();
                ba = null;
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }
        }
        #endregion


        #region Tooltips

        private void setTooltips() {
            ToolTip tooltip= new ToolTip();
            tooltip.SetToolTip(this.textBox1, "Select a Folder");
            tooltip.SetToolTip(this.textBox2, "Select a Folder");
            tooltip.SetToolTip(this.button1, "Select a Folder");
            tooltip.SetToolTip(this.button6, "Select a Folder");
            tooltip.SetToolTip(this.button2, "Reset to standard folder");
            tooltip.SetToolTip(this.button7, "Reset to standard folder");
            tooltip.SetToolTip(this.button11, "Open current folder");
            tooltip.SetToolTip(this.button12, "Open current folder");
            tooltip.SetToolTip(this.checkBox1, "Check this box if you want to start this program on Windows startup");
            tooltip.SetToolTip(this.checkBox4, "Check this box if you want to start this program minimized");
            tooltip.SetToolTip(this.checkBox2, "Check this box if you want to start the backup on program startup");
            tooltip.SetToolTip(this.checkBox3, "Check this box if you want to save a daily backup");
            tooltip.SetToolTip(this.checkBox5, "Check this box if you want to save a weekly backup");
            tooltip.SetToolTip(this.label6, "Sets the maximum number of interval backups");
            tooltip.SetToolTip(this.numericUpDown2, "Sets the maximum number of interval backups");
            tooltip.SetToolTip(this.label3, "Sets the timer for the interval backup");
            tooltip.SetToolTip(this.numericUpDown1, "Sets the timer for the interval backup");
            tooltip.SetToolTip(this.label5, "Set the compression level of the backup files");
            tooltip.SetToolTip(this.comboBox1, "Set the compression level of the backup files");
            tooltip.SetToolTip(this.button10, "Performs a manual backup");
            tooltip.SetToolTip(this.button8, "Starts the interval timer");
            tooltip.SetToolTip(this.button9, "Stops the interval timer");
            tooltip.SetToolTip(this.button5, "Saves your settings");
            tooltip.SetToolTip(this.button3, "Saves the settings and minimize the program into the system tray");
            tooltip.SetToolTip(this.button4, "Close the program");

        }

        #endregion
    }
}
