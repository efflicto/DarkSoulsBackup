﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DarkSoulsBackup.utils {
    class Backup {

        #region Main backup functions
        /// <summary>
        /// Does a manual backup
        /// </summary>
        public void doManualBackup() {
            try {
                Debug.WriteLine("Manual backup started.");
                string savePath = DarkSoulsBackup.Properties.Settings.Default.savePath;
                if(Directory.Exists(savePath)) {
                    if(savePath.Contains("%appdata%")) {
                        savePath = savePath.Replace("%appdata%\\", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
                    }
                    string backupPath = DarkSoulsBackup.Properties.Settings.Default.backupPath;
                    string lastBackupFile = doBackup(backupPath: backupPath, savePath: savePath, type: "manual");
                } else {
                    MessageBox.Show("Error folder not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Does the main backup task
        /// IF the user checked daily or weekly backup it performs them
        /// After that it performs the inteval backup
        /// </summary>
        public void doAutomaticBackup() {
            try {
                Debug.WriteLine("Backup started");
                string savePath = DarkSoulsBackup.Properties.Settings.Default.savePath;
                if(Directory.Exists(savePath)) {
                    if(savePath.Contains("%appdata%")) {
                        savePath = savePath.Replace("%appdata%\\", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
                    }
                    string backupPath = DarkSoulsBackup.Properties.Settings.Default.backupPath;

                    if(DarkSoulsBackup.Properties.Settings.Default.backupKeepDaily) {
                        this.checkDaily(backupPath: backupPath, savePath: savePath);
                    }
                    if(DarkSoulsBackup.Properties.Settings.Default.backupKeepWeekly) {
                        this.checkWeekly(backupPath: backupPath, savePath: savePath);
                    }
                    string lastIntervalBackupFile = this.doBackup(backupPath: backupPath, savePath: savePath, type: "interval");
                } else {
                    MessageBox.Show("Error folder not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Performs the backup
        /// Either daily, weekly or in a interval
        /// </summary>
        /// <param name="backupPath"></param>
        /// <param name="savePath"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private string doBackup(string backupPath, string savePath, string type) {
            try {
                //use the Ionic Zlib
                ZipFile zip = new ZipFile();
                zip.CompressionLevel = DarkSoulsBackup.Properties.Settings.Default.commpressionLevel;
                zip.CompressionMethod = DarkSoulsBackup.Properties.Settings.Default.compressionMethod;
                zip.ParallelDeflateThreshold = 0;
                string backupFile = "";
                List<FileData> backupFiles = new List<FileData>();
                //Get all files in game save folder and add them to the zip file object
                backupFiles.AddRange(getFiles(path: savePath, fileExtension: "*.*", onlyTopDir: false));
                Debug.WriteLine("Compressing...");
                foreach(FileData file in backupFiles) {
                    try {
                        zip.AddFile(file.Path, file.Path.Replace(file.Name, ""));
                    } catch(Exception) {
                    }
                }
                //Writes the zip file object to the disk for the selected backup type
                string backupDate = DateTime.Now.ToString().Replace(".", "").Replace(" ", "_").Replace(":", "");
                switch(type) {
                    case "manual":
                        Debug.WriteLine("manual backup");
                        zip.Save(backupPath + "\\" + "ManualBackup_" + backupDate + ".zip");
                        backupFile = backupPath + "\\" + "ManualBackup_" + backupDate + ".zip";
                        break;
                    case "daily":
                        Debug.WriteLine("daily backup");
                        zip.Save(backupPath + "\\" + "DailyBackup_" + DateTime.Today.ToShortDateString().Replace(".", "") + ".zip");
                        backupFile = backupPath + "\\" + "DailyBackup_" + DateTime.Today.ToShortDateString().Replace(".", "") + ".zip";
                        break;
                    case "weekly":
                        Debug.WriteLine("weekly backup");
                        zip.Save(backupPath + "\\" + "WeeklyBackup_" + DateTime.Today.ToShortDateString().Replace(".", "") + ".zip");
                        backupFile = backupPath + "\\" + "WeeklyBackup_" + DateTime.Today.ToShortDateString().Replace(".", "") + ".zip";
                        break;
                    case "interval":
                        Debug.WriteLine("interval backup");
                        Debug.WriteLine(backupDate);
                        List<FileData> intervalBackupFiles = new List<FileData>();
                        intervalBackupFiles.AddRange(getFiles(path: backupPath, fileExtension: "Interval*.zip", onlyTopDir: true));
                        if(intervalBackupFiles.Count == 0) {

                            zip.Save(backupPath + "\\" + "IntervalBackup_" + backupDate + ".zip");
                            backupFile = backupPath + "\\" + "IntervalBackup_" + backupDate + ".zip";
                            break;
                        } else {
                            if(intervalBackupFiles.Count + 1 > DarkSoulsBackup.Properties.Settings.Default.backupCount) {

                                List<FileData> sortedIntervalBackupFiles = new List<FileData>();

                                sortedIntervalBackupFiles.AddRange(intervalBackupFiles.OrderBy(x => x.LastWriteTime));

                                while(sortedIntervalBackupFiles.Count + 1 > DarkSoulsBackup.Properties.Settings.Default.backupCount) {
                                    Debug.WriteLine("Deleting file " + sortedIntervalBackupFiles[0].Path);
                                    System.IO.File.Delete(sortedIntervalBackupFiles[0].Path);
                                    sortedIntervalBackupFiles.RemoveAt(0);
                                    sortedIntervalBackupFiles.OrderBy(x => x.LastWriteTime);
                                }
                            }
                            zip.Save(backupPath + "\\" + "IntervalBackup_" + backupDate + ".zip");
                            backupFile = backupPath + "\\" + "IntervalBackup_" + backupDate + ".zip";
                            break;
                        }
                    default:
                        break;
                }

                Debug.WriteLine("Done!");
                return backupFile;
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return "Error";
            }

        }
        #endregion


        #region Check functions
        /// <summary>
        /// Checks the weekly backup file.
        /// It creates a weekly backup file if no daily backup file is present
        /// If the weekly backup file is older than 6 days it generates a new one and deletes the old one
        /// </summary>
        /// <param name="backupPath"></param>
        /// <param name="savePath"></param>
        private void checkWeekly(string backupPath, string savePath) {
            try {
                Debug.WriteLine("Checking weekly backup files");
                List<FileData> backupFiles = new List<FileData>();
                backupFiles.AddRange(getFiles(path: backupPath, fileExtension: "Weekly*.zip", onlyTopDir: true));
                string lastBackupFile = "";

                if(backupFiles.Count == 0) {
                    Debug.WriteLine("No weekly backups. Creating one...");
                    lastBackupFile = doBackup(backupPath: backupPath, savePath: savePath, type: "weekly");
                    DarkSoulsBackup.Properties.Settings.Default.lastWeekly = DateTime.UtcNow;
                    DarkSoulsBackup.Properties.Settings.Default.Save();
                    this.cleanupFiles(type: "Weekly", backupPath: backupPath, lastBackupFile: lastBackupFile);
                } else {
                    List<FileData> sortedIntervalBackupFiles = new List<FileData>();
                    sortedIntervalBackupFiles.AddRange(backupFiles.OrderBy(x => x.LastWriteTime));

                    while(sortedIntervalBackupFiles.Count > 1) {
                        Debug.WriteLine("Deleting file " + sortedIntervalBackupFiles[0].Path);
                        System.IO.File.Delete(sortedIntervalBackupFiles[0].Path);
                        sortedIntervalBackupFiles.RemoveAt(0);
                        sortedIntervalBackupFiles.OrderBy(x => x.LastWriteTime);
                    }
                    if((DateTime.Now - DarkSoulsBackup.Properties.Settings.Default.lastWeekly).Days > 0) {
                        lastBackupFile = doBackup(backupPath: backupPath, savePath: savePath, type: "weekly");
                        DarkSoulsBackup.Properties.Settings.Default.lastWeekly = DateTime.UtcNow;
                        DarkSoulsBackup.Properties.Settings.Default.Save();
                        this.cleanupFiles(type: "Weekly", backupPath: backupPath, lastBackupFile: lastBackupFile);
                    }
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Checks the daily backup file
        /// It creates a daily backup file if no daily backup file is present
        /// If the daily backup file is older than 0 days it generates a new one and deletes the old one
        /// </summary>
        /// <param name="backupPath"></param>
        /// <param name="savePath"></param>
        private void checkDaily(string backupPath, string savePath) {
            try {
                Debug.WriteLine("Checking daily backup files");
                List<FileData> backupFiles = new List<FileData>();
                backupFiles.AddRange(getFiles(path: backupPath, fileExtension: "Daily*.zip", onlyTopDir: true));
                string lastBackupFile = "";

                if(backupFiles.Count == 0) {
                    Debug.WriteLine("No daily backup. Creating one...");
                    lastBackupFile = doBackup(backupPath: backupPath, savePath: savePath, type: "daily");
                    DarkSoulsBackup.Properties.Settings.Default.lastDaily = DateTime.UtcNow;
                    DarkSoulsBackup.Properties.Settings.Default.Save();
                    this.cleanupFiles(type: "Daily", backupPath: backupPath, lastBackupFile: lastBackupFile);
                } else {
                    List<FileData> sortedIntervalBackupFiles = new List<FileData>();
                    sortedIntervalBackupFiles.AddRange(backupFiles.OrderBy(x => x.LastWriteTime));

                    while(sortedIntervalBackupFiles.Count > 1) {
                        Debug.WriteLine("Deleting file " + sortedIntervalBackupFiles[0].Path);
                        System.IO.File.Delete(sortedIntervalBackupFiles[0].Path);
                        sortedIntervalBackupFiles.RemoveAt(0);
                        sortedIntervalBackupFiles.OrderBy(x => x.LastWriteTime);
                    }
                    if((DateTime.Now - DarkSoulsBackup.Properties.Settings.Default.lastDaily).Days > 0) {
                        lastBackupFile = doBackup(backupPath: backupPath, savePath: savePath, type: "daily");
                        DarkSoulsBackup.Properties.Settings.Default.lastDaily = DateTime.UtcNow;
                        DarkSoulsBackup.Properties.Settings.Default.Save();
                        this.cleanupFiles(type: "Daily", backupPath: backupPath, lastBackupFile: lastBackupFile);
                    }
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        #endregion


        #region Helper functions
        /// <summary>
        /// Cleans either the weekly or the daily file. Keeps the last backup file.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="backupPath"></param>
        /// <param name="lastBackupFile"></param>
        private void cleanupFiles(string type, string backupPath, string lastBackupFile) {
            try {
                List<FileData> backupFiles = new List<FileData>();
                string fileExtension = type + "*.zip";
                backupFiles.AddRange(getFiles(path: backupPath, fileExtension: fileExtension, onlyTopDir: true));
                int days = 6;
                switch(type) {
                    case "Daily":
                        days = 0;
                        break;
                    case "Weekly":
                        days = 6;
                        break;
                    default:
                        break;
                }
                foreach(FileData file in backupFiles) {
                    if((DateTime.Now - file.LastWriteTime).Days > days) {
                        if(!file.Path.Equals(lastBackupFile)) {
                            Debug.WriteLine("File " + file.Name + " is " + (DateTime.Now - file.LastWriteTime).Days + " days old. Deleting...");
                            System.IO.File.Delete(file.Path);
                        } else {
                            Debug.WriteLine("File " + file.Name + " is the last backup file.");
                        }
                    }
                }
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Get files in a path with a given file extension (or name)
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileExtension"></param>
        /// <param name="onlyTopDir"></param>
        /// <returns></returns>
        private List<FileData> getFiles(string path, string fileExtension, bool onlyTopDir) {
            try {
                List<FileData> files = new List<FileData>();
                try {
                    if(onlyTopDir) {
                        files.AddRange(FastDirectoryEnumerator.EnumerateFiles(path, fileExtension, SearchOption.TopDirectoryOnly));

                    } else {
                        files.AddRange(FastDirectoryEnumerator.EnumerateFiles(path, fileExtension, SearchOption.AllDirectories));
                    }

                } catch(Exception ex) {

                    Debug.WriteLine(ex.Message);
                }
                return files;
            } catch(Exception err) {
                MessageBox.Show(string.Format("Error: {0}", err.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return null;
            }
        }
        #endregion


    }
}
